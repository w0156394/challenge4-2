<!DOCTYPE html>
		
<html>
	<head>
		<title>Student time planner</title>
		<link rel='stylesheet' type='text/css' href='StyleSheet.css' />
	</head>
	<body>
		<?
			
			$testOutput = "";
			if ($testOutput == "") {
				//If not isset -> set with dumy value 
				if (!isset($_POST['first_name'])) {$_POST['first_name'] = "";} 
				if (!isset($_POST['last_name'])) {$_POST['last_name'] = "";} 
				if (!isset($_POST['birth_year'])) {$_POST['birth_year'] = "";} 
				if (!isset($_POST['grade'])) {$_POST['grade'] = "";} 
				if (!isset($_POST['sibling_number'])) {$_POST['sibling_number'] = "";} 
				if (!isset($_POST['bed_time'])) {$_POST['bed_time'] = "";} 
				if (!isset($_POST['wakeup_time'])) {$_POST['wakeup_time'] = "";} 
				if (!isset($_POST['hours_homework'])) {$_POST['hours_homework'] = "";} 
				if (!isset($_POST['tv_time'])) {$_POST['tv_time'] = "";} 
				if (!isset($_POST['computer_time'])) {$_POST['computer_time'] = "";} 
				if (!isset($_POST['family_time'])) {$_POST['family_time'] = "";} 
				if (!isset($_POST['freind_time'])) {$_POST['freind_time'] = "";} 
				$totalHomeworkHours = 0;
				$totalComGameHours = 0;
				$percentageOfScreenTime = 0;
			} 
			
			$form_block = "
					<form method=\"POST\" action=\"$_SERVER[PHP_SELF]\">
					<p><strong>What is your first name?</strong><br>
					<input type=\"text\" name=\"first_name\" value=\"$_POST[first_name]\" size=30></p>
					<p><strong>What is your last name?</strong><br>
					<input type=\"text\" name=\"last_name\" value=\"$_POST[last_name]\" size=30></p>
					<p><strong>What is the year of your birth?</strong><br>
					<input type=\"text\" name=\"birth_year\" value=\"$_POST[birth_year]\" size=30></p>
					<p><strong>What is your current School Year (7-12)?</strong><br>
					<input type=\"text\" name=\"grade\" value=\"$_POST[grade]\" size=30></p>					
					<p><strong>What is the number of siblings you have?</strong><br>
					<input type=\"text\" name=\"sibling_number\" value=\"$_POST[sibling_number]\" size=30></p>
					<p><strong>When is your bed time (in hours on 24 hour clock)?</strong><br>
					<input type=\"text\" name=\"bed_time\" value=\"$_POST[bed_time]\" size=30></p>
					<p><strong>When do you wake up each day (in hours on 24 hour clock)?</strong><br>
					<input type=\"text\" name=\"wakeup_time\" value=\"$_POST[wakeup_time]\" size=30></p>
					<p><strong>How many hours do you spend on homework each day?</strong><br>
					<input type=\"text\" name=\"hours_homework\" value=\"$_POST[hours_homework]\" size=30></p>
					<p><strong>How many hours do you spend watching TV/DVDs each day?</strong><br>
					<input type=\"text\" name=\"tv_time\" value=\"$_POST[tv_time]\" size=30></p>
					<p><strong>How many hours do you spend on a computer or game console each day?</strong><br>
					<input type=\"text\" name=\"computer_time\" value=\"$_POST[computer_time]\" size=30></p>
					<p><strong>How many hours do you spend with family each day?</strong><br>
					<input type=\"text\" name=\"family_time\" value=\"$_POST[family_time]\" size=30></p>
					<p><strong>How many hours do you spend with freinds each day?</strong><br>
					<input type=\"text\" name=\"freind_time\" value=\"$_POST[freind_time]\" size=30></p>								
					<input type=\"hidden\" name=\"op\" value=\"ds\">
					<p><input type=\"submit\" name=\"submit\" value=\"Send This Form\"></p>
					</form>";
				
			$testOutput = "David Terry's Survey results \n
					What is your name? $_POST[first_name]  $_POST[last_name] \n
					What is the year of your birth? $_POST[birth_year] \n					
					What is your current School Year (7-12)? $_POST[grade] \n					
					What is the number of siblings you have? $_POST[sibling_number] \n
					When is your bed time? $_POST[bed_time] \n
					When do you wake up each day? $_POST[wakeup_time] \n					
					How many hours do you spend on homework each day? $_POST[hours_homework] \n
					How many hours do you spend watching TV/DVDs each day? $_POST[tv_time] \n					
					How many hours do you spend on a computer or game console each day? $_POST[computer_time] \n
					How many hours do you spend with family each day? $_POST[family_time] \n
					How many hours do you spend with freinds each day? $_POST[freind_time] \n
					Hours you will spend doing homework: $totalHomeworkHours \n
					Hours you will spend in front of a screen: $totalComGameHours \n
					Percentage of awake time spent in front of a screen: $percentageOfScreenTime \n";		

			if ($testOutput != "" ){
				$totalHomeworkHours = $_POST['hours_homework'] * 365 * (13 - $_POST['grade']);
				$totalComGameHours = ($_POST['tv_time'] + $_POST['computer_time']) * 365 * (13 - $_POST['grade']);
				$percentageOfScreenTime = ($_POST['tv_time'] + $_POST['computer_time']) / ($_POST['bed_time'] - $_POST['wakeup_time']) * 100;
			}

					
			$output = "
					Great! Thanks $_POST[first_name] for responding to our survey<br>
					Name: $_POST[first_name]  $_POST[last_name]<br>
					Year of your birth: $_POST[birth_year]<br>					
					Current School Year (7-12): $_POST[grade]<br>					
					Number of siblings: $_POST[sibling_number]<br>
					Hours you will spend doing homework: $totalHomeworkHours <br>
					Hours you will spend in front of a screen: $totalComGameHours <br>
					Percentage of awake time spent in front of a screen: $percentageOfScreenTime <br>";
					
					
			if (($_POST['first_name'] = "") ||
				($_POST['last_name'] = "") ||
				($_POST['grade'] = "") ||
				($_POST['sibling_number'] = "") ||
				($_POST['bed_time'] = "") ||
				($_POST['wakeup_time'] = "") ||
				($_POST['hours_homework'] = "") ||
				($_POST['tv_time'] = "") ||
				($_POST['computer_time'] = "") ||
				($_POST['family_time'] = "") ||
				($_POST['freind_time'] = "") ||
				($_POST['birth_year'] = "") ) {
					// they need to see the form
					echo "$form_block";
			} else {
				

					// it's ok to send, so build the mail
					$msg = $testOutput; //"E-MAIL SENT FROM it.nscctruro.ca\n";
					//$msg .= "Sender's Name:    $_POST[sender_name]\n";
					//$msg .= "Sender's E-Mail:  $_POST[sender_email]\n";
					//$msg .= "Message:          $_POST[message]\n\n";
					$msg .= "\n \n Time:          ";
					$msg .= date("F j, Y, g:i a");       
			
					$to = "mcnuttpaulaj@gmail.com";
					$subject = "David Terry's Survey results";
					$mailheaders = "From:it.nscctruro.ca 
					<webmaster@nscctruro.ca>\n";
					//$mailheaders .= "Reply-To: $_POST[sender_email]\n";
					//$mailheaders .= "CC: $_POST[sender_email]";;
					//send the mail
					mail($to, $subject, $msg, $mailheaders);
					//display confirmation to user
					//echo "<p>Mail has been sent!</p>";

					//print error messages
					echo "$form_block";
				 
				 //echo "$testOutput";
				 echo "$output";
			}
		?>
	</body>
</html>